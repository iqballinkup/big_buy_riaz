

import 'package:bigbuy/Custom/Custom_Drtawer/Drawer_all_Section/change_mobile_no.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/Drawer_all_Section/change_password.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/Drawer_all_Section/invite.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/Drawer_all_Section/my_coupon.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/Drawer_all_Section/my_order.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/Drawer_all_Section/my_profile.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/Drawer_all_Section/my_wallet.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/Drawer_all_Section/update_profile_page.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/custom_item_section.dart';
import 'package:bigbuy/custom_page_view.dart';
import 'package:bigbuy/screens/Login_SignUp/pages/signin_page.dart';
import 'package:bigbuy/screens/home/home.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Custom_Drawer_Page extends StatefulWidget {
  const Custom_Drawer_Page({Key? key}) : super(key: key);

  @override
  State<Custom_Drawer_Page> createState() => _Custom_Drawer_PageState();
}

class _Custom_Drawer_PageState extends State<Custom_Drawer_Page> {
  @override
  Widget build(BuildContext context) {

    double size=20;
    double sizee=20;
    return SafeArea(
      child: Drawer(
        backgroundColor: Colors.blue[50],
        child: Container(
          height: double.infinity,
          width: 200,
          child: SingleChildScrollView(
            child:Column(
              children: [
                Container(
                  height: 80,width: double.infinity,
                  color: Colors.blue[50],
                  padding: EdgeInsets.only(
                    left: 20,
                    right: 20
                  ),
                  alignment: Alignment.center,
                  child: Row(
                    children: [
                      CircleAvatar(
                        radius:35,
                        backgroundImage: AssetImage("assets/icons/me.jpg"),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        height: double.infinity,
                        alignment: Alignment.center,
                        child: Column(mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Uzzal Biswas",style: GoogleFonts.poppins(fontSize: 16,fontWeight: FontWeight.w500),),
                            Text("01518657125",style: GoogleFonts.poppins(fontSize: 16,fontWeight: FontWeight.w500),),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Custom_Item_Section(Name: "Home", onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => CustomPageView(selectedIndex: 0),));
                }, icon: Icon(Icons.home,size: size,), iconn: Icon(Icons.arrow_forward_ios,size: sizee,),),

                // Custom_Item_Section(Name: "My Dashboard", onTap: (){
                //   Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen(),));
                // }, icon: Icon(Icons.dashboard,size: size,), iconn: Icon(Icons.arrow_forward_ios,size: sizee,),),

                Custom_Item_Section(Name: "My Dashboard", onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => MyProfile(),));
                }, icon: Icon(Icons.account_circle_outlined,size: size,), iconn: Icon(Icons.arrow_forward_ios,size: sizee,),),
                Custom_Item_Section(Name: "Update Profile", onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => UpdateProfile(),));
                }, icon: Icon(Icons.lock,size: size,), iconn: Icon(Icons.arrow_forward_ios,size: sizee,),),
                Custom_Item_Section(Name: "Change Password", onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => ChangePassword(),));
                }, icon: Icon(Icons.visibility_off,size: size,), iconn: Icon(Icons.arrow_forward_ios,size: sizee,),),
                Custom_Item_Section(Name: "Change Mobile No", onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => ChangeMobile(),));
                }, icon: Icon(Icons.call,size: size,), iconn: Icon(Icons.arrow_forward_ios,size: sizee,),),
                Custom_Item_Section(Name: "My Order", onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => MyOrder(),));
                }, icon: Icon(Icons.shopping_cart,size: size,), iconn: Icon(Icons.arrow_forward_ios,size: sizee,),),
                Custom_Item_Section(Name: "My Wallet", onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => MyWallet(),));
                }, icon: Icon(Icons.wallet,size: size,), iconn: Icon(Icons.arrow_forward_ios,size: sizee,),),
                Custom_Item_Section(Name: "My Coupon List", onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => MyCoupon(),));
                }, icon: Icon(Icons.shopping_bag,size: size,), iconn: Icon(Icons.arrow_forward_ios,size: sizee,),),
                Custom_Item_Section(Name: "Invite", onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Invite(),));
                }, icon: Icon(Icons.insert_invitation,size: size,), iconn: Icon(Icons.arrow_forward_ios,size: sizee,),),

                 InkWell(
                  onTap:  (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => SignInPage(
                      emphone: "01",
                      isRemembered: false,
                    ),));
                  },
                  child: Container(
                    margin: EdgeInsets.only(
                      left: 5,right: 5,top: 5
                    ),
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                    ),
                    padding: EdgeInsets.only(
                        left: 10,right: 10
                    ),
                    height: 50,
                    width: MediaQuery.of(context).size.width,

                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Icon(Icons.logout_outlined),
                            SizedBox(
                              width: 10,
                            ),
                            Text("Log out",style: GoogleFonts.poppins(fontSize: 16,fontWeight: FontWeight.w500),),
                          ],
                        ),

                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
