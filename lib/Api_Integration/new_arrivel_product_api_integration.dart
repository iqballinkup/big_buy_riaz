import 'dart:convert';

import 'package:bigbuy/API_Model_Class/all_product_model_class.dart';
import 'package:bigbuy/constants.dart';
import 'package:dio/dio.dart';

class Api_Hot_Deal_Product_Integration{

  static Future<dynamic> NewArrivel(context)async{
    List<All_Product_Model> NewArrivel_Product=[];
    try{
      String link="${Baseurl}get_web_products";
      Response response=await Dio().post(link,data:
      {
        "is_new_arrival":true,
        "limit":9
      },
      );
      print("HOTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT");
       print(response.data);
      print("HOTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT");
      if(response.statusCode==200){
        final item=jsonDecode(response.data);
        All_Product_Model all_product_model;
        for(var i in item){
          all_product_model=All_Product_Model.fromJson(i);
          NewArrivel_Product.add(all_product_model);
          print(NewArrivel_Product);
        }
      }
    }catch(e){print("Hot Deal Catch Error is $e");}
    return NewArrivel_Product;
  }
  //////////////////////////////////////////////HOT DEAL pRODUCT//////////
  static Future<dynamic> getHotDeal(context)async{
    List<All_Product_Model> hotdeal_list=[];
    try{
      String link="${Baseurl}get_web_products";
      Response response=await Dio().post(link,data:{"is_hot_deals":true});
      print("HOTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT");
      // print(response.data);
      print("HOTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT");
      if(response.statusCode==200){
        final item=jsonDecode(response.data);
        All_Product_Model all_product_model;
        for(var i in item){
          all_product_model=All_Product_Model.fromJson(i);
          hotdeal_list.add(all_product_model);

        }
      }
    }catch(e){print("Hot Deal Catch Error is $e");}
    return hotdeal_list;
  }
}