
import 'package:flutter/material.dart';

class f_section extends StatelessWidget {
    f_section({Key? key,required this.Cashback,required this.name,required this.quantity,required this.price,required this.currencyAmount,required this.rowsize}) : super(key: key);
   String ? name, currencyAmount,price;
   double ?rowsize;
   String ? quantity,Cashback;
  @override
  Widget build(BuildContext context) {
    double totalvalu=double.parse(price!);
    return Container(
      margin: EdgeInsets.only(
        left: 10,
        right: 10,
      ),

      decoration: BoxDecoration(
        border:Border.all(
          width: 1,
          color: Colors.black,
        )
      ),
      child: Column(
        children: [
          Container(
            height:60,
            width: double.infinity,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                    flex:3,
                    child: Container(
                        margin: EdgeInsets.only(left: 5),
                        child: Text("${name}"))),
                Expanded(
                  flex:2,
                  child: Container(
                    margin: EdgeInsets.only(
                      left: 10
                    ),
                    child: Text("qty:${quantity}",style: TextStyle(
                    fontSize: 16
                  ),),

                  ),
                ),
                Expanded(
                  flex:2,
                  child: Container(child: Text("${ price}"),),
                ),
              ],
            ),
          ),
          Divider(thickness: 1,height:1,indent: 1,color: Colors.black54,),
          Container(
            height: rowsize,
            width: double.infinity,
            padding: EdgeInsets.only(
              left: 5,
              right: 5,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Subtotal"),
                Text("${ price}"),
              ],
            ),
          ),
          Divider(thickness: 1,height:1,indent: 1,color: Colors.black54,),
          Container(
            padding: EdgeInsets.only(
              left: 5,
              right: 5,
            ),
            height: rowsize,
            width: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Cashback"),
                Text("${Cashback}"),
              ],
            ),
          ),
          Divider(thickness: 1,height:1,indent: 1,color: Colors.black54,),
          Container(
            padding: EdgeInsets.only(
              left: 5,
              right: 5,
            ),
            height: rowsize,
            width: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("vat+"),
                Text("0.00"),
              ],
            ),
          ),
          Divider(thickness: 1,height:1,indent: 1,color: Colors.black54,),
          Container(
            padding: EdgeInsets.only(
              left: 5,
              right: 5,
            ),
            height: rowsize,
            width: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Delivery Charge"),
                Text("${ currencyAmount}"),
              ],
            ),
          ),
          Divider(thickness: 1,height:1,indent: 1,color: Colors.black54,),
          Container(
            padding: EdgeInsets.only(
              left: 5,
              right: 5,
            ),
            height: rowsize,
            width: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Total"),
                Text("${totalvalu}"),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
