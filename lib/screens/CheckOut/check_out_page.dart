import 'package:bigbuy/Custom/Bootom_NavigationBar/bootom_navigation_bar.dart';
import 'package:bigbuy/CustomTextFormField/custom_text_form_field.dart';
import 'package:bigbuy/screens/CheckOut/Forr_all_checkOut/CustomCheckoutButton/customcheckout_button.dart';
import 'package:bigbuy/screens/CheckOut/Forr_all_checkOut/f_section.dart';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_fonts/google_fonts.dart';


class CheckOutPage extends StatefulWidget {
  CheckOutPage({Key? key,
    required this.mainImage,
    required this.mainPrice,
    required this.cashbackAmount,
    required  this.productName,
    this.qty,
    this.subCategoryId,
    required   this.productCode,
    this.status,
    this.ipAddress,
    this.addBy,
    this.addTime,
    this.brand,
    this.brandName,
    this.cashbackPercent,
    this.isFeatured,
    this.isHotDeals,
    this.isNewArrival,
    this.productBranchid,
    this.productCategoryID,
    this.productCategoryName,
    required this.productDescription,
    this.productShippingReturns,
    this.productSlNo,
    this.productSubCategoryName,
    this.productSubSubCategoryName,
    required this.salePrice,
    this.slug,
    this.stock,
    this.subSubCategoryId,
    this.unitID,
    this.unitName,
    this.updateBy,
    this.updateTime,

  }) : super(key: key);
  String? productSlNo;
  String? productCode;
  String? productName;
  String? slug;
  String? productCategoryID;
  String? subCategoryId;
  String? subSubCategoryId;
  String? brand;
  String? salePrice;
  String? mainPrice;
  String? cashbackPercent;
  String? cashbackAmount;
  String? productDescription;
  String? productShippingReturns;
  String? stock;
  String? unitID;
  String? mainImage;
  String? isFeatured;
  String? isHotDeals;
  String? isNewArrival;
  String? status;
  String? addBy;
  String? addTime;
  String? updateBy;
  String? updateTime;
  String? ipAddress;
  String? productBranchid;
  String? qty;
  String? productCategoryName;
  String? productSubCategoryName;
  String? productSubSubCategoryName;
  String? brandName;
  String? unitName;

  @override
  State<CheckOutPage> createState() => _CheckOutPageState();
}

class _CheckOutPageState extends State<CheckOutPage> {

  final box=GetStorage();

  TextEditingController _nameController=TextEditingController();
  TextEditingController _phonController=TextEditingController();
  TextEditingController _AddressController=TextEditingController();
  TextEditingController _emailController=TextEditingController();
  TextEditingController _ordernotController=TextEditingController();




  FatchUser()async{

    try{
      Map<String , dynamic> token={
        "Content_type":"application/json",
        "Authorization":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYm9ybm9uYmQuY29tXC9hcGlcL2xvZ2luIiwiaWF0IjoxNjc0NDY5NTQxLCJleHAiOjE2NzQ0NzMxNDEsIm5iZiI6MTY3NDQ2OTU0MSwianRpIjoiMUtiaXZnTmkxdUJGbGhTcyIsInN1YiI6MzEsInBydiI6IjFkMGEwMjBhY2Y1YzRiNmM0OTc5ODlkZjFhYmYwZmJkNGU4YzhkNjMifQ.HGYT44pY7x09jte6ys9I46pvJFIJZSZHSKpIiUaxozw",
      };
      final ApiLink="https://bornonbd.com/api/just-order?customer_name=uzzal biswas&customer_mobile=01518657125&customer_email=uzzal.171.cse@gmail.com&billing_address=Dhaka1229&note=proveide in right time&products[0][id]=1&products[0][color_id]=2&products[0][size_id]=2&products[0][product_code]=51551&products[0][name]=pant&products[0][price]=200&products[0][category_id]=4&products[0][subcategory_id]=5&products[0][brand_id]=4&products[0][quantity]=2";
      final response=Dio().post(
          ApiLink,
          // data: {
          //   "customer_name":"uzzal kumar biswas",
          //   "customer_mobile":01518657125,
          //   "customer_email":"uzzal.171.cse@gmail.com",
          //   "billing_address":"Dhjaka, Khilgoan",
          //   "note":"Give me fast",
          //   "products[0][id]":35,
          //   "products[0][color_id]":1,
          //   "products[0][size_id]":1,
          //   "products[0][product_code]":3433,
          //   "products[0][name]":"Baby Lotion",
          //   "products[0][price]":2000,
          //   "products[0][category_id]":5,
          //   "products[0][subcategory_id]":3,
          //   "products[0][brand_id]":1,
          //   "products[0][quantity]":1,
          //
          // },
          options: Options(
            headers: token,
          )
      );
  print(response);
    }catch(e){
      print("catch error ${e}");
    }

  }





  @override
  Widget build(BuildContext context) {
    print("${box.read("token")}");
    double rowsize=35;
    return SafeArea(
      child: Scaffold(

        bottomNavigationBar: CustomNavigationBarPage(
            Home_color: Colors.grey,
            Produc_tColor: Colors.grey,
            Category_color: Colors.grey,
            setting_Color: Colors.grey),
        appBar: AppBar(
          backgroundColor: Color(0xff415b75),
          leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(Icons.arrow_back)),
          centerTitle: true,
          title: Text(
            "Order Summery",
            style: GoogleFonts.poppins(
              fontSize: 17,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        body: Container(
          height: double.infinity,
          width: double.infinity,
          color: Colors.white,
          child: SingleChildScrollView(
            child: Column(
              children: [
                f_section(Cashback: widget.cashbackAmount,name: widget.productName, quantity:widget.qty, price: widget.salePrice, currencyAmount: dropdownvalue=="Cash on delivery" ?"60":"0", rowsize: rowsize),
                Container(
                  height: 45,
                  width: double.infinity,
                  color: Colors.grey,
                  alignment: Alignment.center,
                  child: Text("Billing Address",style: GoogleFonts.poppins(fontSize: 18),),
                ),
                Container(
                  width: double.infinity,
                  //  color: Colors.purpleAccent,
                  margin: EdgeInsets.only(
                    left: 5,
                    right: 5,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        flex:1,
                        child: Container(

                          decoration: BoxDecoration(
                              color: Colors.amber[100],
                              borderRadius: BorderRadius.only(
                                  bottomRight: Radius.circular(11),
                                  bottomLeft: Radius.circular(11)
                              )
                          ),
                          child: Column(
                            children: [
                              Container(
                                child: CustomTextFormField(
                                  controller: _nameController,
                                  validator: (value) {
                                  },
                                  hintText: "Enter name",
                                  Textdata: "Enter name",
                                ),
                              ),
                              CustomTextFormField(
                                controller: _AddressController,
                                validator: (value) {
                                },
                                hintText: "Billing address",
                                Textdata: "Billing address",
                              ),
                              CustomTextFormField(
                                controller: _emailController,
                                validator: (value) {
                                },
                                hintText: "Enter Email",
                                Textdata: "Enter Email",
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(width: 5,),
                      Expanded(
                        flex:1,
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.blue[100],
                              borderRadius: BorderRadius.only(
                                  bottomRight: Radius.circular(11),
                                  bottomLeft: Radius.circular(11)
                              )
                          ),
                          child: Column(
                            children: [
                              Container(
                                child: CustomTextFormField(
                                  controller: _phonController,
                                  validator: (value) {
                                  },
                                  hintText: "Mobile NO :",
                                  Textdata: "Enter Mobile",

                                ),
                              ),
                              CustomTextFormField(
                                controller: _ordernotController,
                                validator: (value) {
                                },
                                hintText: "Order note",
                                Textdata: "Order note",
                              ),
                              Container(
                                height: 30,
                                width: double.infinity,
                                alignment: Alignment.centerLeft,
                                child: Text("Selet your Option",style:TextStyle(
                                  color: Colors.black54,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                ),),
                              ),
                              Container(
                                height: 50,
                                alignment: Alignment.center,
                                width: double.infinity,
                                margin: EdgeInsets.only(top: 5),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    border: Border.all(
                                      width: 3,
                                    )
                                ),
                                child:  DropdownButton(
                                  value: dropdownvalue,
                                  icon: const Icon(Icons.keyboard_arrow_down),
                                  items: items.map((String items) {
                                    return DropdownMenuItem(
                                      value: items,
                                      child: Text(items),
                                    );
                                  }).toList(),
                                  onChanged: (String? newValue) {
                                    setState(() {
                                      dropdownvalue = newValue!;
                                    });
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                InkWell(
                    onTap: () {
                     showDialog(context: context, builder: (context) {
                       return AlertDialog(title: Text("It will be done after somtime"),);
                     },);
                    },
                    child: Custom_Checkout_Button()),

              ],
            ),
          ),
        ),
      ),
    );
  }
  var items = [
    'Cash on delivery',
    'Collect from shop',
    'Courier',
  ];
  String dropdownvalue="Cash on delivery";
}
