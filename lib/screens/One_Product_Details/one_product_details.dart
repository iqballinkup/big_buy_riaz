import 'package:bigbuy/Custom/Bootom_NavigationBar/bootom_navigation_bar.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/custom_drawer.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/end_drawer.dart';
import 'package:bigbuy/FooterSection/about_section.dart';
import 'package:bigbuy/HiveDemoWithCart/product.dart';
import 'package:bigbuy/custom_page_view.dart';
import 'package:bigbuy/screens/CheckOut/check_out_page.dart';
import 'package:bigbuy/screens/CheckOut/riaz_check_out_page.dart';
import 'package:bigbuy/screens/One_Product_Details/custom_rtelated_product_section.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:intl/intl.dart';

class One_Product_Details extends StatefulWidget {
  One_Product_Details({
    Key? key,
    required this.mainImage,
    required this.mainPrice,
    required this.cashbackAmount,
    required this.productName,
    required this.qty,
    this.subCategoryId,
    required this.productCode,
    this.status,
    this.ipAddress,
    this.addBy,
    this.addTime,
    this.brand,
    this.brandName,
    this.cashbackPercent,
    this.isFeatured,
    this.isHotDeals,
    this.isNewArrival,
    this.productBranchid,
    this.productCategoryID,
    this.productCategoryName,
    required this.productDescription,
    this.productShippingReturns,
    this.productSlNo,
    this.productSubCategoryName,
    this.productSubSubCategoryName,
    required this.salePrice,
    this.slug,
    this.stock,
    this.subSubCategoryId,
    this.unitID,
    this.unitName,
    this.updateBy,
    this.updateTime,
  }) : super(key: key);
  String? productSlNo;
  String? productCode;
  String? productName;
  String? slug;
  String? productCategoryID;
  String? subCategoryId;
  String? subSubCategoryId;
  String? brand;
  String? salePrice;
  String? mainPrice;
  String? cashbackPercent;
  String? cashbackAmount;
  String? productDescription;
  String? productShippingReturns;
  String? stock;
  String? unitID;
  String? mainImage;
  String? isFeatured;
  String? isHotDeals;
  String? isNewArrival;
  String? status;
  String? addBy;
  String? addTime;
  String? updateBy;
  String? updateTime;
  String? ipAddress;
  String? productBranchid;
  String? qty;
  String? productCategoryName;
  String? productSubCategoryName;
  String? productSubSubCategoryName;
  String? brandName;
  String? unitName;
  @override
  State<One_Product_Details> createState() => _One_Product_DetailsState();
}

class _One_Product_DetailsState extends State<One_Product_Details> {
  final _key = GlobalKey<ScaffoldState>();
  Box? box = Hive.box('productBox');
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) => CustomPageView(selectedIndex: 0),
                ));
          },
          child: Icon(
            Icons.home,
            size: 27,
          )),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar:
          CustomNavigationBarPage(Home_color: Colors.white, Produc_tColor: Colors.black, Category_color: Colors.white, setting_Color: Colors.white),
      key: _key,
      endDrawer: End_Add_to_cart_Drawer(),
      drawer: Custom_Drawer_Page(),
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back,
            size: 25,
            color: Colors.black87,
          ),
        ),
        title: Text(
          "BigBuy",
          style: GoogleFonts.poppins(
            fontSize: 18,
            fontStyle: FontStyle.italic,
            letterSpacing: 1,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        actions: [
          IconButton(
            iconSize: 28,
            icon: Icon(
              Icons.search,
              size: 25,
              color: Colors.black87,
            ),
            onPressed: () {},
          ),
          IconButton(
            iconSize: 28,
            icon: Icon(
              Icons.shopping_cart,
              size: 25,
              color: Colors.black87,
            ),
            onPressed: () {
              _key.currentState!.openEndDrawer();
            },
          ),
        ],
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        padding: EdgeInsets.only(
          left: 15,
          right: 15,
        ),
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              Container(
                height: 300,
                width: double.infinity,
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: Colors.red,
                          image: DecorationImage(image: AssetImage("assets/icons/me.png"), fit: BoxFit.fill),
                        ),
                        child: Image.network(
                          "https://bigbuy.com.bd/uploads/products/small_image/${widget.mainImage}",
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      width: 100,
                      child: ListView.builder(
                        itemCount: 6,
                        itemBuilder: (context, index) {
                          return Container(
                            height: 70,
                            width: 70,
                            margin: EdgeInsets.only(
                              left: 2,
                              top: 5,
                            ),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              image: DecorationImage(image: AssetImage("assets/icons/me.jpg"), fit: BoxFit.fill),
                            ),
                            child: Image.network(
                              "https://bigbuy.com.bd/uploads/products/small_image/${widget.mainImage}",
                              fit: BoxFit.fill,
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: 30,
                width: double.infinity,
                alignment: Alignment.centerLeft,
                child: Text(
                  "${widget.productName}",
                  style: GoogleFonts.poppins(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              ////////////////////////Product Code ////////
              Row(
                children: [
                  Text(
                    "Code : ",
                    style: GoogleFonts.poppins(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      letterSpacing: 1,
                    ),
                  ),
                  Text("${widget.productCode}"),
                ],
              ),
              ////////////////////////Product Price ////////
              Row(
                children: [
                  Text("Price : ",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        letterSpacing: 1,
                      )),
                  Text("৳${widget.salePrice}",
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w400,
                        letterSpacing: 0,
                        color: Colors.red,
                      )),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    "৳${widget.mainPrice}",
                    style: TextStyle(
                      decoration: TextDecoration.lineThrough,
                      fontSize: 17,
                      fontWeight: FontWeight.w300,
                      letterSpacing: 0,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
              ////////////////////////Product Color ////////
              Container(
                child: Row(
                  children: [
                    Text("Color : ",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          letterSpacing: 1,
                        )),
                    Expanded(
                      child: Container(
                        height: 70,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: Colorss.length,
                          itemBuilder: (context, index) {
                            return Container(
                              margin: EdgeInsets.all(2),
                              height: 65,
                              width: 65,
                              color: Color(Colorss[index]),
                            );
                          },
                        ),
                      ),
                    )
                  ],
                ),
              ),
              ////////////////////////Product Size ////////
              Container(
                margin: EdgeInsets.only(top: 10),
                child: Row(
                  children: [
                    Text("Size : ",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          letterSpacing: 1,
                        )),
                    Expanded(
                      child: Container(
                        height: 30,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: Sizelist.length,
                          itemBuilder: (context, index) {
                            return Container(
                              margin: EdgeInsets.only(left: 10),
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    isSize = index;
                                  });
                                },
                                child: CircleAvatar(
                                  backgroundColor: isSize == index ? Colors.pink : Colors.blue,
                                  radius: 15,
                                  child: Text(
                                    '${Sizelist[index]}',
                                    style: TextStyle(fontSize: 12),
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              ////////////////////////Product Quantity ////////
              Container(
                margin: EdgeInsets.only(top: 5),
                height: 35,
                width: double.infinity,
                // color: Colors.pink,
                alignment: Alignment.center,
                child: Row(
                  children: [
                    Text(
                      "Quantity :",
                      style: GoogleFonts.poppins(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                        letterSpacing: 1,
                      ),
                    ),
                    Row(
                      children: [
                        IconButton(
                            onPressed: () {
                              setState(() {
                                if (countt == 0) {
                                  countt = 0;
                                } else {
                                  countt--;
                                }
                              });
                            },
                            icon: Icon(
                              Icons.remove_circle_outline_outlined,
                              color: Colors.grey,
                            )),
                        Text("${countt}"),
                        IconButton(
                            onPressed: () {
                              setState(() {
                                countt++;
                              });
                            },
                            icon: Icon(
                              Icons.add_circle_outline,
                              color: Colors.grey,
                            )),
                      ],
                    ),
                  ],
                ),
              ),
              //////////////////////// Buy section or Add to cart /////////////////
              Container(
                margin: EdgeInsets.only(left: 10, right: 10, top: 10),
                height: 40,
                width: double.infinity,
                alignment: Alignment.center,
                child: Row(
                  children: [
                    Expanded(
                        child: Container(
                      height: 40,
                      decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          )),
                      alignment: Alignment.center,
                      child: InkWell(
                        onTap: () {
                          bool found = false;
                          setState(() {
                            box!.length;
                          });
                          for (int i = 0; i < box!.length; i++) {
                            ProductDetails existingProduct = box!.getAt(i);
                            if (existingProduct.productName == widget.productName &&
                                existingProduct.productPrice!.toStringAsFixed(2) == widget.salePrice) {
                              existingProduct.productQuantity = int.parse("${widget.qty}");
                              box!.putAt(i, existingProduct);
                              found = true;
                              break;
                            }
                          }
                          if (!found) {
                            ProductDetails productDetails = ProductDetails(
                              // produColor: 0xff01568,
                              // productsize: "18.00",
                              productQuantity: int.parse("${widget.qty}"),
                              productImage: "https://bigbuy.com.bd/uploads/products/small_image/${widget.mainImage}",
                              productName: "${widget.productName}",
                              productPrice: double.parse("${widget.salePrice}"),
                              //new dummy values added here
                              cashback_percent: "100%",
                              color_id: "1",
                              main_price: 100.0,
                              product_id: "1",
                              size_id: "1",

                            );
                            box!.add(productDetails);
                          }
                        },
                        child: Text(
                          "Add to Cart",
                          style: TextStyle(fontSize: 16, color: Colors.white),
                        ),
                      ),
                    )),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          // Navigator.push(
                          //     context,
                          //     MaterialPageRoute(
                          //       builder: (context) => CheckOutPage(
                          //         qty: "${countt}",
                          //         productCode: widget.productCode,
                          //         salePrice: widget.salePrice,
                          //         productDescription: widget.productDescription,
                          //         mainImage: widget.mainImage,
                          //         mainPrice: widget.mainPrice,
                          //         productName: widget.productName,
                          //         cashbackPercent: widget.cashbackPercent,
                          //         cashbackAmount: widget.cashbackAmount,
                          //       ),
                          //     ));
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => CheckoutScreen(
                                  checkoutFrom: "productDetailsPage",
                                  productName: "T-Shirt",
                                  productQuantity: 10,
                                  productSubtotalPrice: 100,
                                  productTotalPrice: 150,
                                  productUnitPrice: 150,
                                ),
                              ));
                        },
                        child: Container(
                          height: 40,
                          decoration: BoxDecoration(
                              color: Colors.pink,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                bottomRight: Radius.circular(20),
                              )),
                          alignment: Alignment.center,
                          child: Text(
                            "Buy now",
                            style: TextStyle(fontSize: 16, color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              //////////////////////// Description Section ////////////////
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Description : ",
                    style: GoogleFonts.poppins(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      letterSpacing: 1,
                    ),
                  ),
                  Divider(
                    height: 1,
                    indent: 1,
                    thickness: 1,
                    color: Colors.black,
                    endIndent: 1,
                  ),
                  Container(
                      child: Text(
                    Bidi.stripHtmlIfNeeded("${widget.productDescription}"),
                    style: TextStyle(fontSize: 12),
                  )),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              //////////////////////// Related Product Section ////////////////
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Related Product : ",
                    style: GoogleFonts.poppins(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      letterSpacing: 1,
                    ),
                  ),
                  Divider(
                    height: 1,
                    indent: 1,
                    thickness: 1,
                    color: Colors.black,
                    endIndent: 1,
                  ),
                ],
              ),
              //////////////////////// Related Product Section ////////////////
              Container(
                margin: EdgeInsets.only(top: 10),
                width: double.infinity,
                child: GridView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    mainAxisSpacing: 16,
                    crossAxisSpacing: 16,
                    mainAxisExtent: 170,
                  ),
                  itemCount: 5,
                  scrollDirection: Axis.vertical,
                  itemBuilder: (context, index) {
                    return Custom_Related_Product_Section();
                  },
                ),
              ),
              //////////////////////About Section/////////////////////////////\
              SizedBox(
                height: 20,
              ),
              BigBuyFooter(),
            ],
          ),
        ),
      ),
    );
  }

  List Colorss = [
    0xffe455e5,
    0xff6EE3C8FF,
    0xffEC1364FF,
  ];
  List Sizelist = [
    "XL",
    "M",
    "S",
  ];
  int? isSize = 0;
  int countt = 1;
}
