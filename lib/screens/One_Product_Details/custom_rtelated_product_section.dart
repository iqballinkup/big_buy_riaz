import 'package:flutter/material.dart';

class Custom_Related_Product_Section extends StatelessWidget {
  const Custom_Related_Product_Section({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Color(0xFFeeeeee),
              ),
              child: Stack(
                children: [
                  Image.asset(
                      "images/american_express.png",
                      width: 182,
                      height: 182),
                  Positioned(
                      top: 3,
                      right: 10,
                      child: CircleAvatar(
                        radius: 13,
                        backgroundColor: Colors.pink,
                        child: Text("25%",style: TextStyle(fontSize: 11),),
                      )
                  ),

                ],
              ),
            ),
          ),
          SizedBox(height: 12),
          Container(
            padding: EdgeInsets.only(
                left: 3,
                right: 3
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Text(
                    "Men  Sleeve Shirt",
                    style: const TextStyle(
                      color: Color(0xFF212121),
                      fontWeight: FontWeight.w500,
                      fontSize: 12,
                    ),
                  ),
                ),
                SizedBox(height: 5),
                Row(
                  children: [
                    Text(
                      'Price :',
                      style: TextStyle(
                          fontSize: 11,
                          fontWeight: FontWeight.bold,
                          color: Color(0xFF212121)),
                    ),
                    Text(
                      '৳1200',
                      style: TextStyle(
                          fontSize: 11,
                          fontWeight: FontWeight.w500,
                          color: Color(0xFF212121)),
                    ),
                  ],
                ),
                SizedBox(height: 3),
                Container(
                  height: 22,
                  width: double.infinity,
                  child: Row(
                    children: [
                      Expanded(
                        child: InkWell(
                          onTap: () {
                            showDialog(context: context, builder: (context) {
                              return AlertDialog(title: Text("Nothing will be Add to cart"),);
                            },);
                          },
                          child: Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              // color: Colors.pink,
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                    width: 1,
                                    color: Colors.pink
                                )
                            ),
                            child: Text(
                              "Add cart",
                              style: TextStyle(color: Colors.pink,fontSize: 11),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: InkWell(
                          onTap: () {
                            showDialog(context: context, builder: (context) {
                              return AlertDialog(title: Text("Nothing will be buy now"),);
                            },);
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                  color: Color(0xff3399FF),
                                  width: 1,
                                )
                            ),
                            alignment: Alignment.center,
                            child: Text(
                              "buy now",
                              style: TextStyle(color: Color(0xff3399FF),fontSize: 11),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),

        ],
      ),
    );
  }
}
