class Brand_List_Model_Class {
  String? _brandSiNo;
  String? _brandName;
  String? _image;
  String? _status;
  String? _brandBranchid;

  Brand_List_Model_Class(
      {String? brandSiNo,
        String? brandName,
        String? image,
        String? status,
        String? brandBranchid}) {
    if (brandSiNo != null) {
      this._brandSiNo = brandSiNo;
    }
    if (brandName != null) {
      this._brandName = brandName;
    }
    if (image != null) {
      this._image = image;
    }
    if (status != null) {
      this._status = status;
    }
    if (brandBranchid != null) {
      this._brandBranchid = brandBranchid;
    }
  }

  String? get brandSiNo => _brandSiNo;
  set brandSiNo(String? brandSiNo) => _brandSiNo = brandSiNo;
  String? get brandName => _brandName;
  set brandName(String? brandName) => _brandName = brandName;
  String? get image => _image;
  set image(String? image) => _image = image;
  String? get status => _status;
  set status(String? status) => _status = status;
  String? get brandBranchid => _brandBranchid;
  set brandBranchid(String? brandBranchid) => _brandBranchid = brandBranchid;

  Brand_List_Model_Class.fromJson(Map<String, dynamic> json) {
    _brandSiNo = json['brand_SiNo'];
    _brandName = json['brand_name'];
    _image = json['image'];
    _status = json['status'];
    _brandBranchid = json['brand_branchid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['brand_SiNo'] = this._brandSiNo;
    data['brand_name'] = this._brandName;
    data['image'] = this._image;
    data['status'] = this._status;
    data['brand_branchid'] = this._brandBranchid;
    return data;
  }
}
